#ifndef __HTTP_PARSE__
#define __HTTP_PARSE__

enum http_method {
   HTTP_GET,
   HTTP_UNSUPPORTED,
};


#define MAX_TARGET_SIZE 1024

typedef struct
{
   enum http_method method;
   int http_major;
   int http_minor;
   char target[MAX_TARGET_SIZE];
} http_request;

int parse_http_request(const char *request_line , http_request *request);

#endif
