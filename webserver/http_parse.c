
#include <string.h>
#include <stdio.h>

#include "http_parse.h"

#define min(a,b) ((a) < (b) ? (a) : (b))
#define in_range(a,b,c) ((a) < (b) ? 0 : ((a) > (c) ? 0 : 1))

int parse_http_request(const char *request_line , http_request *request)
{

   if (strncmp(request_line, "GET ", 4) != 0)
   {
      request->method = HTTP_UNSUPPORTED;
      return 0;
   }
   request->method = HTTP_GET;
   const char *target = strchr(request_line, ' ');
   if (target == NULL)
      return 0;
   target++;
   char *target_end = strchr(target, ' ');
   if (target_end == NULL)
      return 0;
   int size = min(target_end - target, MAX_TARGET_SIZE);
   strncpy(request->target, target, size);

   request->target[size] = '\0';

   char *version = target_end + 1;
   if (strncmp(version, "HTTP/", 5) != 0)
      return 0;
   if (!in_range(version[5], '0', '9')) 
      return 0;
   if (version[6] != '.') 
      return 0;
   if (!in_range(version[7], '0', '9')) 
      return 0;
   request->http_major = version[5] - '0';
   request->http_minor = version[7] - '0';
   return 1;
}

#ifdef COMPILE_MAIN
int main(int argc, char **argv)
{
   if (argc != 2)
   {
      fprintf(stderr, "usage: %s http_request_line\n", argv[0]);
      return 1;
   }

   http_request r;
   if (!parse_http_request(argv[1], &r))
   {
      fprintf(stderr, "Fails to parse request\n");
      if (r.method == HTTP_UNSUPPORTED)
         fprintf(stderr, "Unsupported method\n");
      return 1;
   }
   printf("request line: %s\n", argv[1]);
   printf("method: %s\n", r.method == HTTP_GET ? "GET" : "UNSUPPORTED");
   printf("target: %s\n", r.target);
   printf("version: %d.%d\n", r.http_major, r.http_minor);
   return 0;
}

#endif
